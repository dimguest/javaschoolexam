package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    Stack<Double> stack = new Stack<>();
    String postfix;
    public Calculator() {
        stack = new Stack<>();
        postfix = "";
    }
    public String evaluate(String statement) {
    try{
        PolishNotation convertor = new PolishNotation(statement); //создаем переменную для перевода в польскую запись приводим полученную строку в польскую запись
        postfix = convertor.doTrans(); // приводим полученную строку в польскую запись
        double val;
        double tmpResult = 0; // результат
        double n1, n2; // переменые для расчета
        if (postfix.isEmpty()) return null; // выражение не правильное
        String[] tmp = postfix.split(" ");
        for (int j = 0; j < tmp.length; j++) {
            if (tmp[j].isEmpty())
                return null;
            // если нет операторов
            if (!tmp[j].equals("+") && !tmp[j].equals("-") &&
                    !tmp[j].equals("*") && !tmp[j].equals("/")) {
                try {
                    val = Double.valueOf(tmp[j]);
                } catch (NumberFormatException ex) {
                    //если не цифра возвращается null
                    return null;
                }
                stack.push(val);
            } else {
                //происходит расчет
                n2 = Double.valueOf(stack.pop());
                n1 = Double.valueOf(stack.pop());
                if (tmp[j].equals("+")) {
                    tmpResult = n1 + n2;
                }
                if (tmp[j].equals("-")) {
                    tmpResult = n1 - n2;
                }
                if (tmp[j].equals("*")) {
                    tmpResult = n1 * n2;
                }
                if (tmp[j].equals("/")) {
                    //деление на ноль
                    if (n2 == 0)
                        return null;
                    tmpResult = n1 / n2;
                }
                stack.push(tmpResult);
            }
        }
        //округление на выход и замена "," на "."
        double result= stack.pop();
        String res;
        if(result%1==0){

           return res = Long.toString(Math.round(result));
        }
        else{
           res=Double.toString(result);
           return res.replace(',','.');
        }


        }
    catch (Exception  v){return null;}
        }


}
