package com.tsystems.javaschool.tasks.calculator;


import java.util.Stack;

/**
 * Данный класс преобразует входящую строку к виду обратной польской натации.
 * Обра́тная по́льская запись (англ. Reverse Polish notation, RPN) — форма записи математических и логических выражений, в которой операнды расположены перед знаками операций. Также именуется как обратная польская запись, обратная бесскобочная запись, постфиксная нотация, бесскобочная символика Лукасевича, польская инверсная запись, ПОЛИЗ.
 * Стековой машиной называется алгоритм, проводящий вычисления по обратной польской записи (см. ниже пример вычисления выражений).
 *
 */
public class PolishNotation {

    private Stack<Character> tS; // колл стэк
    private String it; // входная строка
    private String output = ""; // вывод

    public PolishNotation(String in)
    {
        it = in;
        int sS = it.length();
        tS = new Stack<>();
    }

    public String doTrans() // переводим выражение в польскую запись
    {
        int j = 0;
        while (j < it.length()) {
            char ch = it.charAt(j);
            if(ch == ' ') {
                j++;
                continue;
            }
            if(ch == '+' || ch == '-'){
                output += ' ';
                getOper(ch, 1);   // 1ый приоритет операции
                j++;
                continue;
            }
            if(ch == '*' || ch == '/'){
                output += ' ';
                getOper(ch, 2);   // 2ой приоритет операции
                j++;
                continue;
            }
            if(ch == '('){
                tS.push(ch);
                j++;
                continue;
            }
            if(ch == ')'){
                int res = gotParen(ch);
                if(res == 0) return "";
            } else {
                output += ch;   // запись на выход
            }


            j++;
        }
        while (true)
        {
            if (tS.isEmpty()) break;
            output += ' ';
            output += tS.pop(); // запись на вывод
        }
        return output;    // вывод польской записи
    }

    public void getOper(char opThis, int prec1) {
        // достаем оператора их входной строки
        while (true) {
            if (tS.isEmpty()) {
                break;
            }
            char opTop = tS.pop();
            if (opTop != '(') {                        // сравниваем приоритет
                int prec2;
                if (opTop == '+' || opTop == '-')
                    prec2 = 1;
                else
                    prec2 = 2;
                if (prec2 < prec1){
                    tS.push(opTop);
                    break;
                }else{
                    output = output + opTop + ' ';
                }
            } else {          // если это правая скобка'('
                tS.push(opTop);
                break;
            }
        }
        tS.push(opThis);
    }


    public int gotParen(char ch) {
        if(tS.size() < 2) return 0;
        while (true) {
            if (tS.isEmpty()) break;
            char chx = tS.pop();
            if (chx != '(') {
                output += ' ';
                output += chx;
            } else {
                break;
            }
        }
        return 1;
    }
}
