package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.*;

public class PyramidBuilder{

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        try{ Integer[] a= new Integer[inputNumbers.size()];
            a= (Integer[]) inputNumbers.toArray();
         sort(a);
     double k= -0.5+0.5*Math.sqrt(1+8*inputNumbers.size());
     int count =0;
     if(k%1==0)
     {

         int columns = (int)(k+(k-1));
         int rows=(int)k;
         int[][] result= new int[rows][columns];
         for(int i=0;i<rows;i++){
             for(int j=0; j<columns;j++){
                 result[i][j]=0;
             }
         }
         for(int i = 0; i<rows;i++){
             for(int j = rows-1-i; j<=rows-1+i; j = j+2){
               result[i][j]=a[count];
               count++;
             }
         }

         return result;
     }
     else{throw new CannotBuildPyramidException();}
        }
        catch (Throwable ex){throw new CannotBuildPyramidException(); }
    }


}
